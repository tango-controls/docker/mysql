# mysql docker image

Docker image with MySQL and TANGO DB Scheme

## Usage

```bash
docker run --rm -it -e MYSQL_ROOT_PASSWORD=root registry.gitlab.com/tango-controls/docker/mysql
```

## Local Development

```bash
docker build -t tango-db .
docker run -e MYSQL_ROOT_PASSWORD=root --rm -i -t tango-db
```
